const PayamSMS = require("payamsms-sdk");

require("dotenv").config();
const env = process.env;

const sms = new PayamSMS(
  env.PAYAM_ORGANIZATION,
  env.PAYAM_USERNAME,
  env.PAYAM_PASSWORD,
  env.PAYAM_LINE
);

const SendSMS = (req, res) => {
  const { messages } = req.body;

  const send = sms.send(messages);

  send.then((result) => {
    const sent = result[0];

    if (sent.code === 200) res.status(200).send({ message: "SMS sent" });
    else res.status(400).send({ message: "SMS did not sent" });
  });
};

const Balance = (req, res) => {
  const balance = sms.getBalance();

  balance.then((result) => {
    res.send(result);
  });
};

module.exports = {
  SendSMS,
  Balance,
};
