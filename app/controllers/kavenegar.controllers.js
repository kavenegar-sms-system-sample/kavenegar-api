const Kave = require("../models/kavenegar.model");

const Kavenegar = require("kavenegar");

require("dotenv").config();
const env = process.env;

const kavenegar = Kavenegar.KavenegarApi({ apikey: env.KavenegarToken });
const sender = "10008663";

const SendSMS = (req, res) => {
    const { message, receptor } = req.body;

    kavenegar.Send({ message, receptor, sender }, (response, status) => {
        if (status === 200) {
            const { cost, messageid } = response[0];

            const insertingData = { message, receptor, sender, cost, messageid };
            
            const sms = new Kave(insertingData);
            sms.save()
                .then((result) => res.status(200).send({ message: "SMS sent" }))
                .catch((error) => res.status(500).send({ message: "can not create model" }))
        } else res.status(500).send({ message: `Message did not send with status ${status}`});
    });
}

module.exports = {
    SendSMS,
}