const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const kavenegarSchema = new Schema(
    {
        message: {
            type: String,
            default: "",
            unique: false,
            required: true,
        },
        receptor: {
            type: String,
            default: "",
            unique: false,
            required: true,
        },
        sender: {
            type: String,
            default: "",
            unique: false,
            required: true,
        },
        cost: {
            type: Number,
            default: 0,
            unique: false,
            required: true,
        },
        messageid: {
            type: Number,
            default: 0,
            unique: false,
            required: true,
        },
    }, { timestamps: true },
);

const Kave = mongoose.model('kavenegar', kavenegarSchema);

module.exports = Kave;