const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const payamSchema = new Schema(
    {
        message: {
            type: String,
            default: "",
            unique: false,
            required: true,
        },
        receptor: {
            type: String,
            default: "",
            unique: false,
            required: true,
        },
        sender: {
            type: String,
            default: "",
            unique: false,
            required: true,
        },
        cost: {
            type: Number,
            default: 0,
            unique: false,
            required: true,
        },
        messageid: {
            type: Number,
            default: 0,
            unique: false,
            required: true,
        },
    }, { timestamps: true },
);

const Payam = mongoose.model('payam', payamSchema);

module.exports = Payam;