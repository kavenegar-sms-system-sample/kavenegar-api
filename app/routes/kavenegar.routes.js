const express = require("express");

const controllers = require("../controllers/kavenegar.controllers");

const Router = express.Router();

Router.post("/send", controllers.SendSMS);

module.exports = Router;