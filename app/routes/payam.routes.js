const express = require("express");

const controllers = require("../controllers/payam.controllers");

const Router = express.Router();

Router.post("/send", controllers.SendSMS);
Router.get("/balance", controllers.Balance);

module.exports = Router;
