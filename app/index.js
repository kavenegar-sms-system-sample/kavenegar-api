const express = require("express");
const cors = require("cors");

const v1 = require("./v1");

const app = express();

app.set("json spaces", 2);
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use(cors());

app.use("/v1/sms", v1);

module.exports = app;
