const express = require("express");

const KavenegarRoutes = require("./routes/kavenegar.routes");
const PayamSMSRoutes = require("./routes/payam.routes");

const app = express();

app.use("/kavenegar", KavenegarRoutes);
app.use("/payamsms", PayamSMSRoutes);

module.exports = app;
